<?php

namespace ContextualCode\CalendarBundle\CalendarEventStorage;

use ContextualCode\CalendarBundle\CalendarEvent\CalendarEvent;

class ArrayCalendarEventStorage implements CalendarEventStorageInterface
{

    protected $eventList;

    public function __construct(array $eventList)
    {
        $this->eventList = $eventList;
    }

    /**
     * @inheritdoc
     */
    public function getAllEvents()
    {
        $eventObjectList = array();
        foreach ($this->eventList as $eventArrayItem)
        {
            if (!is_array($eventArrayItem)) {
                continue;
            }

            $eventTimes = array("start_date" => null, "end_date" => null);
            foreach (array_keys($eventTimes) as $eventDateType) {

                if (!array_key_exists($eventDateType, $eventArrayItem)) {
                    continue;
                }

                $date = null;
                switch(gettype($eventArrayItem[$eventDateType])) 
                {
                    case "string":
                    {
                        $unixTime = strtotime($eventArrayItem[$eventDateType]);
                        if ($unixTime !== false) {
                            $date = new \DateTime();
                            $date->setTimestamp($unixTime);
                        }
                        break;
                    }
                    case "integer":
                    {
                        $date = new \DateTime();
                        $date->setTimestamp($eventArrayItem[$eventDateType]);
                        break;
                    }
                    case "object":
                    {
                        if (get_class($eventArrayItem[$eventDateType]) == "DateTime") {
                            $date = $eventArrayItem[$eventDateType];
                        }
                        break;
                    }
                }
                if ($date) {
                    $eventTimes[$eventDateType] = $date;
                }

            }

            $event = new CalendarEvent(
                array_key_exists("id", $eventArrayItem) ? $eventArrayItem["id"] : 0,
                array_key_exists("title", $eventArrayItem) ? $eventArrayItem["title"] : "(untitled)",
                $eventTimes["start_date"],
                $eventTimes["end_date"] ? $eventTimes["start_date"]->diff($eventTimes["end_date"]) : null,
                array_key_exists("rrule", $eventArrayItem) && is_string($eventArrayItem["rrule"]) ? $eventArrayItem["rrule"] : "",
                array_key_exists("groups", $eventArrayItem) && is_array($eventArrayItem["groups"]) ? $eventArrayItem["groups"] : array(),
                $eventArrayItem
            );

            $eventObjectList[] = $event;

        }

        return $eventObjectList;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedEvents()
    {
        return $this->getAllEvents();
    }

}
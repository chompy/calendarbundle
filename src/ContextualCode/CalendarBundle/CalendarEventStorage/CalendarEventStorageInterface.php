<?php

namespace ContextualCode\CalendarBundle\CalendarEventStorage;

use ContextualCode\CalendarBundle\Classes\CalendarFilter;

/**
 * Interface which defines method of event retrival.
 */
interface CalendarEventStorageInterface
{
    /**
     * Get all events from storage interface
     * @return ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface[]
     */
    public function getAllEvents();

    /**
     * Get modified events from storage interface
     * @return ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface[]
     */
    public function getModifiedEvents();

}
<?php

namespace ContextualCode\CalendarBundle\Tests;

use ContextualCode\CalendarBundle\CalendarEventStorage\ArrayCalendarEventStorage;
use ContextualCode\CalendarBundle\Services\CalendarService;
use ContextualCode\CalendarBundle\CalendarView;
use ContextualCode\CalendarBundle\Classes\TestArrayStorage;

class CalendarViewTest extends \PHPUnit_Framework_TestCase
{

    public function testMonthView()
    {
        $calendarEventStorage = new ArrayCalendarEventStorage(TestArrayStorage::$sampleCalendarEventArray);

        $monthView = new CalendarView\MonthCalendarView(
            new \DateTime("12/15/2015 12:00AM EST")
        );

        // ensure start date is beginning of month
        $this->assertEquals(
            "20151201",
            $monthView->getStartDate()->format("Ymd")
        );

        // ensure end date is end of month
        $this->assertEquals(
            "20151231",
            $monthView->getEndDate()->format("Ymd")
        );

        // ensure next page is start of next month
        $this->assertEquals(
            "20160101",
            $monthView->getNextPage()->format("Ymd")
        );

        // ensure previous page is start of previous month
        $this->assertEquals(
            "20151101",
            $monthView->getPrevPage()->format("Ymd")
        );

    }

}
<?php

namespace ContextualCode\CalendarBundle\Tests;

use ContextualCode\CalendarBundle\CalendarEventStorage\ArrayCalendarEventStorage;
use ContextualCode\CalendarBundle\Services\CalendarService;
use ContextualCode\CalendarBundle\CalendarView;
use ContextualCode\CalendarBundle\Classes\CalendarGroupFilter;
use ContextualCode\CalendarBundle\Classes\TestArrayStorage;

class CalendarFilterTest extends \PHPUnit_Framework_TestCase
{

    public function testFilterView()
    {
        $calendarEventStorage = new ArrayCalendarEventStorage(TestArrayStorage::$sampleCalendarEventArray);
        $calendarEventList = $calendarEventStorage->getAllEvents();

        $groupsFilter = new CalendarGroupFilter(CalendarGroupFilter::LOGIC_AND);
        $groupsFilter->addCondition(CalendarGroupFilter::LOGIC_IN, "alpha");
        foreach ($calendarEventList as $event) {
            if (in_array("alpha", $event->getGroups())) {
                $this->assertTrue($groupsFilter->testFilter($event));
            }
        }

        $groupsFilter = new CalendarGroupFilter(CalendarGroupFilter::LOGIC_AND);
        $groupsFilter->addCondition(CalendarGroupFilter::LOGIC_NOT_IN, "beta");
        foreach ($calendarEventList as $event) {
            if (!in_array("beta", $event->getGroups())) {
                $this->assertTrue($groupsFilter->testFilter($event));
            }
        }

    }

}
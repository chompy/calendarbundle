<?php

namespace ContextualCode\CalendarBundle\Tests;

use ContextualCode\CalendarBundle\CalendarEventStorage\ArrayCalendarEventStorage;
use ContextualCode\CalendarBundle\Services\CalendarService;
use ContextualCode\CalendarBundle\Classes\CalendarFilter;
use ContextualCode\CalendarBundle\Classes\TestArrayStorage;
use ContextualCode\CalendarBundle\Classes\EventCompiler;
use ContextualCode\CalendarBundle\CalendarView;

class EventCompilerTest extends \PHPUnit_Framework_TestCase
{

    public function testCompiler()
    {

        // set compile path
        $testPath = sprintf(
            "%s/cc-cal-test",
            sys_get_temp_dir()
        );
        if (!file_exists($testPath)) {
            mkdir(
                sprintf(
                    "%s/cc-cal-test",
                    sys_get_temp_dir()
                )
            );
        }
        EventCompiler::$compilePath = sprintf(
            "%s/cc-cal-test",
            sys_get_temp_dir()
        );

        // delete previous compiled events
        EventCompiler::clear();

        // get all events from storage
        $calendarEventStorage = new ArrayCalendarEventStorage(TestArrayStorage::$sampleCalendarEventArray);
        $eventList = $calendarEventStorage->getAllEvents();        

        // compile events
        $this->assertTrue( EventCompiler::compile($eventList) );

        // ensure last modified date is correct
        $lastModifiedDate = EventCompiler::getLastModified();
        $this->assertEquals(
            $lastModifiedDate->format("Ymd"),
            date("Ymd")
        );

    }

    public function testCompiledEventRetrival()
    {

        // test retrival of events
        $eventList = EventCompiler::getCompiledEvents(
            new \DateTime("2000-01-01"),
            new \DateTime("2010-01-01")
        );
        $this->assertEquals(
            count($eventList),
            1
        );
        $this->assertEquals(
            $eventList[0]->getId(),
            4
        );

        $eventList = EventCompiler::getCompiledEvents(
            new \DateTime("2015-12-01"),
            new \DateTime("2016-12-31")
        );
        $this->assertEquals(
            count($eventList),
            5
        );
        $this->assertEquals(
            $eventList[0]->getId(),
            1
        );
        $this->assertEquals(
            $eventList[3]->getId(),
            4
        );

        $eventList = EventCompiler::getCompiledEvents(
            new \DateTime("2016-01-01"),
            new \DateTime("2017-01-01")
        );
        $this->assertEquals(
            count($eventList),
            4
        );
        $this->assertEquals(
            $eventList[0]->getId(),
            2
        );
        $this->assertEquals(
            $eventList[2]->getId(),
            4
        );

        // month view
        $view = new CalendarView\MonthCalendarView(
            new \DateTime("01/01/2016 12:00 AM")
        );
        $eventList = EventCompiler::getCompiledEvents(
            $view->getStartDate(),
            $view->getEndDate()
        );
        $this->assertEquals(
            count($eventList),
            4
        );
        $this->assertEquals(
            $eventList[3]->getId(),
            5
        );
    }

    public function testCompiledDateRetrival()
    {
        $dateList = EventCompiler::getCompiledDates(
            new \DateTime("2000-01-01"),
            new \DateTime("2010-01-01")
        );
        $this->assertEquals(
            count($dateList),
            1044
        );
        $this->assertEquals(
            $dateList[0][0],
            4
        );
        $this->assertEquals(
            $dateList[0][1]->format("Ymd"),
            "20000104"
        );

        $dateList = EventCompiler::getCompiledDates(
            new \DateTime("2015-12-01"),
            new \DateTime("2015-12-31")
        );
        $this->assertEquals(
            count($dateList),
            15
        );
        $this->assertEquals(
            $dateList[1][0],
            1
        );
        $this->assertEquals(
            $dateList[14][1]->format("Ymd"),
            "20151230"
        );
    }

    public function testCompileModified()
    {

        $eventArray = array(
            array(
                "id" => 1,
                "title" => "Test Event A",
                "start_date" => "02/01/2016 3:00PM EST",
                "end_date" => "02/01/2016 5:00PM EST",
                "groups" => array("A", "alpha")
            ),
            array(
                "id" => 5,
                "title" => "Test Event E",
                "start_date" => "03/12/2016 12:30PM EST",
                "rrule" => "FREQ=MONTHLY",
                "groups" => array("A", "alpha", "B", "beta")
            )
        );

        // get all events from storage
        $calendarEventStorage = new ArrayCalendarEventStorage($eventArray);
        $eventList = $calendarEventStorage->getAllEvents();    

        // compile modified
        $this->assertTrue( EventCompiler::compileModified($eventList) );

        // test retrival of events
        $eventList = EventCompiler::getCompiledEvents(
            new \DateTime("2016-02-01"),
            new \DateTime("2016-02-20")
        );
        $this->assertEquals(
            count($eventList),
            2
        );
        $this->assertEquals(
            $eventList[0]->getId(),
            1
        );

        $eventList = EventCompiler::getCompiledEvents(
            new \DateTime("2016-04-01"),
            new \DateTime("2016-04-20")
        );
        $this->assertEquals(
            count($eventList),
            2
        );
        $this->assertEquals(
            $eventList[0]->getId(),
            5
        );

    }

}
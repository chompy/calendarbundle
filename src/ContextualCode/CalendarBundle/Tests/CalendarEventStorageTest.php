<?php

namespace ContextualCode\CalendarBundle\Tests;

use ContextualCode\CalendarBundle\CalendarEventStorage\ArrayCalendarEventStorage;
use ContextualCode\CalendarBundle\Classes\TestArrayStorage;

class CalendarEventStorageTest extends \PHPUnit_Framework_TestCase
{

    public function testArrayCalendarEventStorage()
    {
        $calendarEventStorage = new ArrayCalendarEventStorage(TestArrayStorage::$sampleCalendarEventArray);
        $calendarEventList = $calendarEventStorage->getAllEvents();

        // ensure correct number of events returned
        $this->assertEquals(
            count(TestArrayStorage::$sampleCalendarEventArray),
            count($calendarEventList)
        );

        foreach ($calendarEventList as $key => $event) {
            $this->assertEquals($event->getId(), TestArrayStorage::$sampleCalendarEventArray[$key]["id"]);
            $this->assertEquals($event->getTitle(), TestArrayStorage::$sampleCalendarEventArray[$key]["title"]);
        }

    }
}
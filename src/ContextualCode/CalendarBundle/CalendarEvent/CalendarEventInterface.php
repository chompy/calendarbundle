<?php

namespace ContextualCode\CalendarBundle\CalendarEvent;

interface CalendarEventInterface
{

    /**
     * @return integer
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return DateTime
     */
    public function getStartDate();

    /**
     * @return DateInterval
     */
    public function getEventLength();

    /**
     * @return string
     */
    public function getRruleString();

    /**
     * @return array
     */
    public function getGroups();


}
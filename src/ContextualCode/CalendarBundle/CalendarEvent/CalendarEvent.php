<?php

namespace ContextualCode\CalendarBundle\CalendarEvent;

class CalendarEvent implements CalendarEventInterface
{

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var DateTime
     */
    protected $startDate;

    /**
     * @var DateInterval
     */
    protected $eventLength;

    /**
     * @var string
     */
    protected $rrule;

    /**
     * @var array
     */
    protected $groups;

    /**
     * @var array
     */
    protected $parameters;

    /**
     * @param integer $id
     * @param string $title
     * @param DateTime $startDate
     * @param DateInterval $eventLength
     * @param string $rrule
     * @param array $parameters
     */
    public function __construct(
        $id,
        $title, 
        \DateTime $startDate, 
        \DateInterval $eventLength = null,
        $rrule = "",
        array $groups = array(),
        array $parameters = array()
    ) {
        $this->id = is_integer($id) ? $id : 0;
        $this->title = is_string($title) ? $title : "(untitled)";
        $this->startDate = $startDate;
        $this->eventLength = $eventLength;
        $this->rrule = is_string($rrule) ? $rrule : "";
        $this->groups = $groups;
        $this->parameters = $parameters;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        if (!is_integer($id)) {
            return;
        }
        $this->id = $id;
    }

    /**
     * @inheritdoc
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     */
    public function setTitle($title) {
        if (!is_string($title)) {
            return;
        }
        $this->title = $title;
    }

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param DateTime $startDate
     */
    public function setStartDate(\DateTime $startDate) {
        $this->startDate = $startDate;
    }

    /**
     * @inheritdoc
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param DateInterval $eventLength
     */
    public function setEventLength(\DateInterval $eventLength)
    {
        $this->eventLength = $eventLength;
    }

    /**
     * @inheritdoc
     */
    public function getEventLength()
    {
        return $this->eventLength;
    }

    /**
     * @param string $rrule
     */
    public function setRruleString($rrule)
    {
        if (!is_string($rrule)) {
            return;
        }
        $this->rrule = $rrule;
    }

    /**
     * @inheritdoc
     */
    public function getRruleString()
    {
        return $this->rrule;
    }

    /**
     * @param string
     */
    public function addGroup($value)
    {
        if (!is_string($value) && !is_numeric($value)) {
            return;
        }
        $this->groups[] = $value;
    }

    /**
     * @inheritdoc
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @inheritdoc
     */
    public function getParameter($key)
    {
        if (!array_key_exists($key, $this->parameters)) {
            return;
        }
        return $this->parameters[$key];
    }

}
<?php

namespace ContextualCode\CalendarBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use ContextualCode\CalendarBundle\Classes\EventCompiler;

// test
use ContextualCode\CalendarBundle\Classes\TestArrayStorage;
use ContextualCode\CalendarBundle\CalendarEventStorage\ArrayCalendarEventStorage;

class CompileCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('contextual_code_calendar:compile')
            ->setDescription('Compile all calendar events')
            ->addArgument(
                'storage',
                InputArgument::OPTIONAL,
                'Calendar event storage service'
            )
            ->addOption(
               'clean',
               null,
               InputOption::VALUE_NONE,
               'Perform a clean compilation (wipe all old events).'
            )
            ->addOption(
               'test',
               null,
               InputOption::VALUE_NONE,
               'Compile events in Classes\\TestArrayStorage.'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $startTime = microtime(true);
        $output->writeln("Start event compiler...");

        $container = $this->getApplication()->getKernel()->getContainer();

        if ($input->getOption("test")) {
            $storageService = new ArrayCalendarEventStorage(TestArrayStorage::$sampleCalendarEventArray);
        } else {
            $storageServiceName = $input->getArgument("storage") ?: $this->getApplication()->getKernel()->getContainer()->getParameter("contextual_code_calendar.default_storage");
            if (!$storageServiceName) {
                $output->writeln("<error>ERROR...calendar storage is not available.</error>");
                return;
            }
            $storageService = $container->get($storageServiceName);
        }

        EventCompiler::$compilePath = $container->getParameter("contextual_code_calendar.compile_path");
        if (!EventCompiler::$compilePath || !is_dir(EventCompiler::$compilePath)) {
            EventCompiler::$compilePath = $container->getParameter("kernel.cache_dir");
        }

        if ($input->getOption("clean")) {
            $output->write("Retreiving ALL events...");
            $eventList = $storageService->getAllEvents();
            $output->writeln("done.");
            $output->write("Clearing old compilation...");
            EventCompiler::clear();
            $output->writeln("done.");
            $output->write(sprintf("Compiling %d event(s)...", count($eventList)));
            EventCompiler::compile($eventList);
            $output->writeln("done.");
        } else {
            $output->write("Retreiving MODIFIED events...");
            $eventList = $storageService->getModifiedEvents();
            $output->writeln("done.");
            $output->write(sprintf("Compiling %d event(s)...", count($eventList)));
            EventCompiler::compileModified($eventList);
            $output->writeln("done.");
        }
        $output->writeln(
            sprintf("All done! (%sms)", round(microtime(true) - $startTime, 3))
        );

    }
}
<?php

namespace ContextualCode\CalendarBundle\Classes;

use ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface;

class CalendarGroupFilter
{

    const LOGIC_AND = 0;
    const LOGIC_OR = 1;
    const LOGIC_IN = 20;
    const LOGIC_NOT_IN = 21;

    protected $operator;
    protected $filters;

    public function __construct($operator = self::LOGIC_AND)
    {
        if (!in_array($operator, array(self::LOGIC_AND, self::LOGIC_OR))) {
            throw new \InvalidArgumentException("Calendar group filter operator '{$operator}' is invalid.");
        }
        $this->operator = $operator;
    }

    /**
     * Add logical condition to filter
     * @param string $operator
     * @param mixed $value
     * @return CalendarFilter
     */
    public function addCondition($operator, $value = "")
    {
        if (!in_array($operator, array(self::LOGIC_IN, self::LOGIC_NOT_IN))) {
            throw new \InvalidArgumentException("Calendar group filter conditional operator '{$operator}' is invalid.");
        }
        $this->filters[] = array(
            "operator" => $operator,
            "value" => $value
        );
        return $this;
    }

    /**
     * Add sub filter
     * @param CalendarGroupFilter $filter
     * @return CalendarFilter
     */
    public function addFilter(CalendarGroupFilter $filter)
    {
        $this->filters[] = $filter;
        return $this;
    }

    /**
     * Return array of conditions and sub filters
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Test if given event matches filter
     * @param CalendarEventInterface $event
     * @return boolean
     */
    public function testFilter(CalendarEventInterface $event)
    {
        if (!$this->filters) {
            return true;
        }

        $hasFilters = false;
        if ($this->operator == self::LOGIC_AND) {
            $hasFilters = true;
        }

        foreach ($this->filters as $filter) {
            if (is_a($filter, "CalendarGroupFilter", false)) {
                $hasFilters = $filter->testFilter($event);
            } else {

                $compareValue = $event->getGroups();
                if (!is_array($compareValue)) {
                    continue;
                }

                switch ($filter["operator"])
                {
                    case self::LOGIC_IN:
                    {
                        $hasFilters = in_array($filter["value"], $compareValue);
                        break;
                    }
                    case self::LOGIC_NOT_IN:
                    {
                        $hasFilters = !in_array($filter["value"], $compareValue);
                        break;
                    }
                } 
            }

            if ($this->operator == self::LOGIC_AND && !$hasFilters) {
                return false;
            
            } else if ($this->operator == self::LOGIC_OR && $hasFilters) {
                return true;
            }

        }

        if ($this->operator == self::LOGIC_AND) {
            return true;
        }
        return false;
    }

}
<?php

namespace ContextualCode\CalendarBundle\Classes;

use ContextualCode\CalendarBundle\CalendarEventStorage\CalendarEventStorageInterface;
use ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface;
use ContextualCode\CalendarBundle\CalendarView\CalendarViewInterface;
use ContextualCode\CalendarBundle\Classes\CalendarFilter;
use ContextualCode\CalendarBundle\Services\CalendarService;
use ContextualCode\CalendarBundle\CalendarEventStorage\ArrayCalendarEventStorage;
use ContextualCode\CalendarBundle\Tests\CalendarEventStorageTest;

class TestArrayStorage extends CalendarService
{

    static public $sampleCalendarEventArray = array(
        array(
            "id" => 1,
            "title" => "Test Event A",
            "start_date" => "12/01/2015 3:00PM EST",
            "end_date" => "12/01/2015 5:00PM EST",
            "groups" => array("A", "alpha")
        ),
        array(
            "id" => 2,
            "title" => "Test Event B",
            "start_date" => "11/25/2015 3:00PM EST",
            "end_date" => "11/26/2015 3:00PM EST",
            "rrule" => "FREQ=WEEKLY;COUNT=10",
            "groups" => array("B", "beta")
        ),        
        array(
            "id" => 3,
            "title" => "Test Event C",
            "start_date" => 1453449600,
            "groups" => array("A", "alpha", "test-group")
        ),
        array(
            "id" => 4,
            "title" => "Test Event D",
            "start_date" => "11/01/1980 3:00PM EST",
            "rrule" => "FREQ=WEEKLY;BYDAY=TU,TH",
            "groups" => array("B", "beta")
        ),
        array(
            "id" => 5,
            "title" => "Test Event E",
            "start_date" => 1453748444,
            "end_date" => "01/25/2016 3:00PM EST",
            "rrule" => "FREQ=MONTHLY;"
        )  
    );

    public function __construct()
    {
        $this->calendarEventStorage = new ArrayCalendarEventStorage(self::$sampleCalendarEventArray);
    }

}

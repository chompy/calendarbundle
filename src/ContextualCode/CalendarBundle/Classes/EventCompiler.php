<?php

namespace ContextualCode\CalendarBundle\Classes;

use ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface;
use ContextualCode\CalendarBundle\CalendarEvent\CalendarEvent;
use ContextualCode\CalendarBundle\CalendarEvent\CalendarCompiledEvent;
use ContextualCode\CalendarBundle\Services\CalendarService;

/**
 * Allows compilation of calendar events in to
 * a flat binary file for fast lookups
 */
class EventCompiler
{
    /**
     * Name of file to store compiled events in
     * @var string
     */
    const COMPILED_FILE = "cc-calendar.dat";

    /**
     * Number of years worth of event dates
     * to store in compiled file
     * @var integer
     */
    const COMPILE_YEARS = 200;

    /**
     * Time in years from today that the 
     * compiled file should start 
     * compiling from.
     * @var integer
     */
    const COMPILE_PREV_YEARS = -25;

    /**
     * Packer format for recording string sizes
     */
    const COMPILE_STRING_SIZE_PACK = "n";
    const COMPILE_STRING_SIZE_BYTES = 2;

    /**
     * Packer format for recording event count
     */
    const COMPILE_EVENT_COUNT_PACK = "n";
    const COMPILE_EVENT_COUNT_BYTES = 2;

    /**
     * Packer format for recording group count
     */
    const COMPILE_GROUP_COUNT_PACK = "C";
    const COMPILE_GROUP_COUNT_BYTES = 1;

    /**
     * Packer format for event ids
     */
    const COMPILE_ID_PACK = "N";
    const COMPILE_ID_BYTES = 4;

    /**
     * Packer formats for dates
     */
    const COMPILE_YEAR_PACK = "n";
    const COMPILE_YEAR_BYTES = 2;
    const COMPILE_MONTH_PACK = "C";
    const COMPILE_MONTH_BYTES = 1;
    const COMPILE_DAY_PACK = "C";
    const COMPILE_DAY_BYTES = 1;
    const COMPILE_HOUR_PACK = "C";
    const COMPILE_HOUR_BYTES = 1;
    const COMPILE_MINUTE_PACK = "C";
    const COMPILE_MINUTE_BYTES = 1;

    /**
     * Packer format for file pointers
     */
    const COMPILE_POINTER_PACK = "N";
    const COMPILE_POINTER_BYTES = 4;

    /**
     * Path to place compiled events in
     * @var string
     */
    static public $compilePath = "";

    /**
     * Return array containing default start and
     * end dates for compiled events
     * @return \DateTime[]
     */
    static public function getDefaultCompilationDateRange()
    {
        return array(
            new \DateTime(self::COMPILE_PREV_YEARS . " years"),
            new \DateTime((self::COMPILE_PREV_YEARS + self::COMPILE_YEARS) . " years")
        );
    }

    /** 
     * Return full path to compile file
     * @return string
     */
    static private function getCompilePath()
    {
        if (!self::$compilePath || !is_dir(self::$compilePath)) {
            return sys_get_temp_dir() . "/" . self::COMPILED_FILE;
        }
        return self::$compilePath . "/" . self::COMPILED_FILE;
    }

    /**
     * Pack a date at current file position
     * @param resource $fh
     * @param DateTime $date
     */
    static private function packDate($fh, \DateTime $date)
    {
        fwrite(
            $fh,
            pack(
                self::COMPILE_YEAR_PACK,
                $date->format("Y")
            ),
            self::COMPILE_YEAR_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_MONTH_PACK,
                $date->format("n")
            ),
            self::COMPILE_MONTH_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_DAY_PACK,
                $date->format("j")
            ),
            self::COMPILE_DAY_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_HOUR_PACK,
                $date->format("G")
            ),
            self::COMPILE_HOUR_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_MINUTE_PACK,
                $date->format("i")
            ),
            self::COMPILE_MINUTE_BYTES
        );
    }

    /**
     * Unpack date at current file position
     * @param resource $fh
     * @return DateTime
     */
    static private function unpackDate($fh)
    {
        $year =  unpack(
            self::COMPILE_YEAR_PACK,
            fread(
                $fh,
                self::COMPILE_YEAR_BYTES
            )
        );
        $month =  unpack(
            self::COMPILE_MONTH_PACK,
            fread(
                $fh,
                self::COMPILE_MONTH_BYTES
            )
        );
        $day =  unpack(
            self::COMPILE_DAY_PACK,
            fread(
                $fh,
                self::COMPILE_DAY_BYTES
            )
        );
        $hour =  unpack(
            self::COMPILE_HOUR_PACK,
            fread(
                $fh,
                self::COMPILE_HOUR_BYTES
            )
        );
        $minute =  unpack(
            self::COMPILE_MINUTE_PACK,
            fread(
                $fh,
                self::COMPILE_MINUTE_BYTES
            )
        );
        return new \DateTime("{$year[1]}-{$month[1]}-{$day[1]} {$hour[1]}:{$minute[1]}");
    }

    /**
     * Pack an event at current file position
     * @param resource $fh
     * @param CalendarEventInterface $event
     */
    static private function packEvent($fh, CalendarEventInterface $event)
    {
        // event id
        fwrite(
            $fh,
            pack(
                self::COMPILE_ID_PACK,
                $event->getId()
            ),
            self::COMPILE_ID_BYTES
        );

        // event name
        fwrite(
            $fh,
            pack(
                self::COMPILE_STRING_SIZE_PACK,
                strlen($event->getTitle())
            ),
            self::COMPILE_STRING_SIZE_BYTES
        );
        fwrite(
            $fh,
            $event->getTitle()
        );

        // event date
        self::packDate($fh, $event->getStartDate());

        // event length
        $eventLength = $event->getEventLength();
        fwrite(
            $fh,
            pack(
                self::COMPILE_YEAR_PACK,
                $eventLength ? $eventLength->y : 0
            ),
            self::COMPILE_YEAR_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_MONTH_PACK,
                $eventLength ? $eventLength->m : 0
            ),
            self::COMPILE_MONTH_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_DAY_PACK,
                $eventLength ? $eventLength->d : 0
            ),
            self::COMPILE_DAY_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_HOUR_PACK,
                $eventLength ? $eventLength->h : 0
            ),
            self::COMPILE_HOUR_BYTES
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_MINUTE_PACK,
                $eventLength ? $eventLength->i : 0
            ),
            self::COMPILE_MINUTE_BYTES
        );

        // groups
        $groups = $event->getGroups();
        fwrite(
            $fh,
            pack(
                self::COMPILE_GROUP_COUNT_PACK,
                count($groups)
            ),
            self::COMPILE_GROUP_COUNT_BYTES
        );       
        foreach ($groups as $group) {
            fwrite(
                $fh,
                pack(
                    self::COMPILE_STRING_SIZE_PACK,
                    strlen($group)
                ),
                self::COMPILE_STRING_SIZE_BYTES
            );
            fwrite(
                $fh,
                $group
            );
        }
    }

    /** 
     * Unpack an event at current file position
     * @param resource $fh
     * @return ContextualCode\CalendarBundle\CalendarEvent\CalendarCompiledEvent
     */
    static private function unpackEvent($fh)
    {
        // get event id
        $id =  unpack(
            self::COMPILE_ID_PACK,
            fread(
                $fh,
                self::COMPILE_ID_BYTES
            )
        )[1];

        // get title length
        $titleLength = unpack(
            self::COMPILE_STRING_SIZE_PACK,
            fread(
                $fh,
                self::COMPILE_STRING_SIZE_BYTES
            )
        )[1];

        // get title
        $title = fread(
            $fh,
            $titleLength
        );

        // get start date
        $startDate = self::unpackDate($fh);

        // get event length
        $eventLength = new \DateInterval("P0Y");
        $eventLength->y = unpack(
            self::COMPILE_YEAR_PACK,
            fread(
                $fh,
                self::COMPILE_YEAR_BYTES
            )
        )[1];
        $eventLength->m = unpack(
            self::COMPILE_MONTH_PACK,
            fread(
                $fh,
                self::COMPILE_MONTH_BYTES
            )
        )[1];
        $eventLength->d = unpack(
            self::COMPILE_DAY_PACK,
            fread(
                $fh,
                self::COMPILE_DAY_BYTES
            )
        )[1];
        $eventLength->h = unpack(
            self::COMPILE_HOUR_PACK,
            fread(
                $fh,
                self::COMPILE_HOUR_BYTES
            )
        )[1];
        $eventLength->i = unpack(
            self::COMPILE_MINUTE_PACK,
            fread(
                $fh,
                self::COMPILE_MINUTE_BYTES
            )
        )[1];

        // get group count
        $groupCount = unpack(
            self::COMPILE_GROUP_COUNT_PACK,
            fread(
                $fh,
                self::COMPILE_GROUP_COUNT_BYTES
            )
        )[1];

        // get groups
        $groups = array();
        for ($i = 0; $i < $groupCount; $i++) {
            $strLength = unpack(
                self::COMPILE_STRING_SIZE_PACK,
                fread(
                    $fh,
                    self::COMPILE_STRING_SIZE_BYTES
                )
            )[1];
            $groups[] = fread(
                $fh,
                $strLength
            );
        }

        // make event
        return new CalendarCompiledEvent(
            $id,
            $title,
            $startDate,
            $eventLength,
            "",
            $groups
        );

    }

    /**
     * Pack index at current file position
     * @param resource $fh
     * @param array $dates (array of arrays containing event id and DateTime)
     */
    static private function packIndex($fh, array $dates)
    {

        // sort
        usort(
            $dates,
            function($a, $b) {
                if ($a[1] < $b[1]) {
                    return -1;
                } else if ($a[1] > $b[1]) {
                    return 1;
                }
                return 0;
            }
        );        

        // calculate length of a single date
        $dateLength = self::COMPILE_ID_BYTES + self::COMPILE_YEAR_BYTES + self::COMPILE_MONTH_BYTES + self::COMPILE_DAY_BYTES + self::COMPILE_HOUR_BYTES + self::COMPILE_MINUTE_BYTES;

        // placeholder for index count
        $indexCountPos = ftell($fh);
        fwrite(
            $fh,
            pack(
                self::COMPILE_EVENT_COUNT_PACK,
                0
            ),
            self::COMPILE_EVENT_COUNT_BYTES
        );

        // interate and pack
        $currentYear = 0;
        $currentMonth = 0;
        $indexCount = 0;
        foreach ($dates as $count => $date) {

            if ($date[1]->format("Y") > $currentYear || $date[1]->format("n") > $currentMonth) {
                $currentYear = $date[1]->format("Y");
                $currentMonth = $date[1]->format("n");

                fwrite(
                    $fh,
                    pack(
                        self::COMPILE_YEAR_PACK,
                        $currentYear
                    ),
                    self::COMPILE_YEAR_BYTES
                );
                fwrite(
                    $fh,
                    pack(
                        self::COMPILE_MONTH_PACK,
                        $currentMonth
                    ),
                    self::COMPILE_MONTH_BYTES
                );
                fwrite(
                    $fh,
                    pack(
                        self::COMPILE_POINTER_PACK,
                        $count
                    ),
                    self::COMPILE_POINTER_BYTES
                );
                $indexCount++;

            }
        }

        // write index count
        $currentPos = ftell($fh);
        fseek($fh, $indexCountPos, SEEK_SET);
        fwrite(
            $fh,
            pack(
                self::COMPILE_EVENT_COUNT_PACK,
                $indexCount
            ),
            self::COMPILE_EVENT_COUNT_BYTES
        );
        fseek($fh, $currentPos, SEEK_SET);

    }

    /**
     * Pack date list at current file position
     * @param resource $fh
     * @param array $dates (array of arrays containing event id and DateTime)
     */
    static private function packDateList($fh, array $dates)
    {
        usort(
            $dates,
            function($a, $b) {
                if ($a[1] < $b[1]) {
                    return -1;
                } else if ($a[1] > $b[1]) {
                    return 1;
                }
                return 0;
            }
        );
        fwrite(
            $fh,
            pack(
                self::COMPILE_POINTER_PACK,
                count($dates)
            ),
            self::COMPILE_POINTER_BYTES
        );
        foreach ($dates as $count => $date) {
            fwrite(
                $fh,
                pack(
                    self::COMPILE_ID_PACK,
                    $date[0]
                ),
                self::COMPILE_ID_BYTES
            );
            self::packDate($fh, $date[1]);
        }
    }

    /**
     * Compile events and dates into a flat file
     * @param ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface[] $events
     * @return boolean Return true if completed
     */
    static public function compile(array $events, $startTime = null, $endTime = null)
    {

        // get full compile path
        $compilePath = self::getCompilePath();

        // should be a file
        if (file_exists($compilePath) && !is_file($compilePath)) {
            return false;
        }

        // use default time ranges if needed
        if (!$startTime) {
            $startTime = self::getDefaultCompilationDateRange()[0];
        }
        if (!$endTime) {
            $endTime = self::getDefaultCompilationDateRange()[1];
        }

        // prepare compilation
        $fh = fopen($compilePath, "wb");

        // record last modified time
        $now = new \DateTime("now");
        self::packDate($fh, $now);

        // get all dates
        $dates = array();
        foreach ($events as $event) {
            if (is_a($event, "CalendarCompiledEvent") || !is_object($event)) {
                continue;
            }
            $eventDates = CalendarService::getEventDates($event, $startTime, $endTime);
            foreach ($eventDates as $date) {
                $dates[] = array($event->getId(), $date);
            }
        }

        // write index
        self::packIndex($fh, $dates);

        // write dates
        self::packDateList($fh, $dates);

        // record event count
        fwrite(
            $fh,
            pack(
                self::COMPILE_EVENT_COUNT_PACK,
                count($events)
            ),
            self::COMPILE_EVENT_COUNT_BYTES
        );

        // iterate events and write event data
        foreach ($events as $event) {
            self::packEvent($fh, $event);
        }

        // close file
        fclose($fh);

        // done
        return true;

    }

    /**
     * Append given events to existing compilation
     * @param ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface[] $events
     * @param DateTime $startTime
     * @param DateTime $endTime
     */
    static public function compileModified(array $events, $startTime = null, $endTime = null)
    {
        // get full compile path
        $compilePath = self::getCompilePath();

        // should be a file
        if (file_exists($compilePath) && !is_file($compilePath)) {
            return false;
        }

        // use default time ranges if needed
        if (!$startTime) {
            $startTime = self::getDefaultCompilationDateRange()[0];
        }
        if (!$endTime) {
            $endTime = self::getDefaultCompilationDateRange()[1];
        }

        // get existing dates
        $dates = self::getCompiledDates();

        // build new array of dates with dates for modified events
        // removed
        $newDates = array();
        $eventIds = array();
        foreach ($events as $event) {
            $eventIds[] = $event->getId();
        }
        foreach ($dates as $date) {
            if (!in_array($date[0], $eventIds)) {
                $newDates[] = $date;
            }
        }

        // add dates for modified events
        foreach ($events as $event) {
            if (is_a($event, "CalendarCompiledEvent") || !is_object($event)) {
                continue;
            }
            $eventDates = CalendarService::getEventDates($event, $startTime, $endTime);
            foreach ($eventDates as $date) {
                $newDates[] = array($event->getId(), $date);
            }
        }

        // get all events
        $previousEvents = self::getCompiledEvents();
        foreach ($previousEvents as $event) {
            $hasEvent = false;
            foreach ($events as $modifiedEvent) {
                if ($modifiedEvent->getId() == $event->getId()) {
                    $hasEvent = true;
                    break;
                }
            }
            if (!$hasEvent) {
                $events[] = $event;
            }
        }

        // prepare compilation
        $fh = fopen($compilePath, "wb");

        // record last modified time
        $now = new \DateTime("now");
        self::packDate($fh, $now);

        // write index
        self::packIndex($fh, $newDates);

        // write dates
        self::packDateList($fh, $newDates);

        // record event count
        fwrite(
            $fh,
            pack(
                self::COMPILE_EVENT_COUNT_PACK,
                count($events)
            ),
            self::COMPILE_EVENT_COUNT_BYTES
        );

        // iterate events and write event data
        foreach ($events as $event) {
            self::packEvent($fh, $event);
        }

        // close file
        fclose($fh);

        // done
        return true;

    }

    /**
     * Deletes the file containing compiled events
     */
    static public function clear()
    {
        $compilePath = self::getCompilePath();
        if (is_file($compilePath)) {
            unlink($compilePath);
        }
    }

    /**
     * Return true if compiled event file exists
     * @return boolean
     */
    static public function has()
    {
        return is_file(self::getCompilePath());
    }

    /**
     * Read last modification time from compiled file
     * @return DateTime
     */
    static public function getLastModified()
    {
        // must exist
        if (!self::has()) {
            return null;
        }

        // get full compile path
        $compilePath = self::getCompilePath();

        // open compilation
        $fh = fopen($compilePath, "rb");

        // read last modified date
        $date = self::unpackDate($fh);

        // close file
        fclose($fh);

        return $date;
    }

    /**
     * Open compiled file with the intent of
     * reading events/dates
     * @return resource|null
     */
    static private function readCompiledFile()
    {

        // must exist
        if (!self::has()) {
            return null;
        }

        // get full compile path
        $compilePath = self::getCompilePath();
        
        // open compilation
        $fh = fopen($compilePath, "rb");

        // skip modification time
        fseek(
            $fh,
            self::COMPILE_YEAR_BYTES + self::COMPILE_MONTH_BYTES + self::COMPILE_DAY_BYTES + self::COMPILE_HOUR_BYTES + self::COMPILE_MINUTE_BYTES,
            SEEK_CUR
        );

        return $fh;

    }

    /**
     * Get list of event dates in given range
     * @param DateTime $from
     * @param DateTime $to
     * @param integer $limit
     * @param resource $fh
     * @return array
     */
    static public function getCompiledDates(\DateTime $from = null, \DateTime $to = null, $limit = -1, $fh = null)
    {

        // create new file resource if non provided
        $providedFh = true;
        if (!$fh) {
            $fh = self::readCompiledFile();
            $providedFh = false;
        }
        // unable to open file
        if (!$fh) {
            return array();
        }
        // default from
        if (!$from) {
            $from = self::getDefaultCompilationDateRange()[0];
        }
        // default to
        if (!$to) {
            $to = self::getDefaultCompilationDateRange()[1];
        }

        // get index count
        $indexCount = unpack(
            self::COMPILE_EVENT_COUNT_PACK,
            fread(
                $fh,
                self::COMPILE_EVENT_COUNT_BYTES
            )
        )[1];

        // iterate index for best date
        $datePos = 0;
        for ($i = 0; $i < $indexCount; $i++) {
            $year = unpack(
                self::COMPILE_YEAR_PACK,
                fread(
                    $fh,
                    self::COMPILE_YEAR_BYTES
                )
            )[1];
            $month = unpack(
                self::COMPILE_MONTH_PACK,
                fread(
                    $fh,
                    self::COMPILE_MONTH_BYTES
                )
            )[1];
            $pointer = unpack(
                self::COMPILE_POINTER_PACK,
                fread(
                    $fh,
                    self::COMPILE_POINTER_BYTES
                )
            )[1];
            if ($year >= $from->format("Y") && $month >= $from->format("n")) {
                $datePos = $pointer;
                fseek(
                    $fh,
                    ($indexCount - $i - 1) * (self::COMPILE_YEAR_BYTES + self::COMPILE_MONTH_BYTES + self::COMPILE_POINTER_BYTES),
                    SEEK_CUR
                );
                break;
            }
        }

        // get date count
        $dateCount = unpack(
            self::COMPILE_POINTER_PACK,
            fread(
                $fh,
                self::COMPILE_POINTER_BYTES
            )
        )[1];

        // jump to date
        fseek(
            $fh,
            $datePos * (self::COMPILE_ID_BYTES + self::COMPILE_YEAR_BYTES + self::COMPILE_MONTH_BYTES + self::COMPILE_DAY_BYTES + self::COMPILE_HOUR_BYTES + self::COMPILE_MINUTE_BYTES),
            SEEK_CUR
        );

        $dates = array();
        for ($i = $datePos; $i < $dateCount; $i++) {
            
            $id = unpack(
                self::COMPILE_ID_PACK,
                fread(
                    $fh,
                    self::COMPILE_ID_BYTES
                )
            )[1];
            $date = self::unpackDate($fh);

            if ($date > $to) {
                fseek(
                    $fh,
                    ($dateCount - $i - 1) * (self::COMPILE_ID_BYTES + self::COMPILE_YEAR_BYTES + self::COMPILE_MONTH_BYTES + self::COMPILE_DAY_BYTES + self::COMPILE_HOUR_BYTES + self::COMPILE_MINUTE_BYTES),
                    SEEK_CUR
                );
                break;
            }
            $dates[] = array(
                $id,
                $date
            );
            if ($limit > 0 && count($dates) >= $limit) {
                break;
            }
        }

        // close file if handler was not passed as argument
        if (!$providedFh) {
            fclose($fh);
        }

        return $dates;

    }

    /**
     * Find events and dates in given range from compiled
     * file
     * @param DateTime $from
     * @param DateTime $to
     * @param integer $limit
     * @param resource $fh
     * @return CalendarCompiledEvent[]
     */
    static public function getCompiledEvents(\DateTime $from = null, \DateTime $to = null, $limit = -1, $fh = null)
    {
        // create new file resource if non provided
        if (!$fh) {
            $fh = self::readCompiledFile();
        }
        // unable to open file
        if (!$fh) {
            return array();
        }

        // default from
        if (!$from) {
            $from = self::getDefaultCompilationDateRange()[0];
        }
        // default to
        if (!$to) {
            $to = self::getDefaultCompilationDateRange()[1];
        }

        // get date list
        $dates = self::getCompiledDates($from, $to, -1, $fh);

        // get event count
        $eventCount = unpack(
            self::COMPILE_EVENT_COUNT_PACK,
            fread(
                $fh,
                self::COMPILE_EVENT_COUNT_BYTES
            )
        )[1];
        $eventPos = ftell($fh);

        // get events
        $events = array();
        for ($i = 0; $i < $eventCount; $i++) {

            // get event id
            $id =  unpack(
                self::COMPILE_ID_PACK,
                fread(
                    $fh,
                    self::COMPILE_ID_BYTES
                )
            )[1];

            // skip if id not in date list
            $hasId = false;
            foreach ($dates as $date) {
                if ($date[0] == $id) {
                    $hasId = true;
                    break;
                }
            }

            // pull event
            if ($hasId) {
                fseek(
                    $fh,
                    -self::COMPILE_ID_BYTES,
                    SEEK_CUR
                );
                $events[] = self::unpackEvent($fh);
                if ($limit > 0 && count($events) >= $limit) {
                    break;
                }

            // skip ahead
            } else {

                // get title length and skip title
                $titleLength = unpack(
                    self::COMPILE_STRING_SIZE_PACK,
                    fread(
                        $fh,
                        self::COMPILE_STRING_SIZE_BYTES
                    )
                )[1];

                // skip to groups
                fseek(
                    $fh,
                    $titleLength + (2 * (self::COMPILE_YEAR_BYTES + self::COMPILE_MONTH_BYTES + self::COMPILE_DAY_BYTES + self::COMPILE_HOUR_BYTES + self::COMPILE_MINUTE_BYTES)),
                    SEEK_CUR
                );

                // get group count
                $groupCount = unpack(
                    self::COMPILE_GROUP_COUNT_PACK,
                    fread(
                        $fh,
                        self::COMPILE_GROUP_COUNT_BYTES
                    )
                )[1];

                // skip over groups
                $groups = array();
                for ($j = 0; $j < $groupCount; $j++) {
                    $strLength = unpack(
                        self::COMPILE_STRING_SIZE_PACK,
                        fread(
                            $fh,
                            self::COMPILE_STRING_SIZE_BYTES
                        )
                    )[1];
                    fseek(
                        $fh,
                        $strLength,
                        SEEK_CUR
                    );
                }

            }

        }

        fclose($fh);

        return $events;

    }

}
<?php

namespace ContextualCode\CalendarBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use ContextualCode\CalendarBundle\Classes\CalendarGroupFilter;
use ContextualCode\CalendarBundle\Classes\TestArrayStorage;
use ContextualCode\CalendarBundle\Services\CalendarService;

class CalendarController
{

    /**
     * Default template to display if view provides none
     * @var string
     */
    const DEFAULT_VIEW_TEMPLATE = "ContextualCodeCalendarBundle::default.html.twig";

    /**
     * Operator to use on calendar filters
     * @var integer
     */
    static public $filterGroupOperator = CalendarGroupFilter::LOGIC_AND;

    /**
     * Aliases for built in calendar views
     * @var array
     */
    static protected $calendarViews = array(
        "month" => "ContextualCode\\CalendarBundle\\CalendarView\\MonthCalendarView"
    );

    /**
     * @var ContextualCode\CalendarBundle\Services\CalendarService
     */
    protected $calendarService;

    /**
     * @var Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
     */
    protected $templating;


    public function __construct(CalendarService $calendarService, EngineInterface $templating)
    {
        $this->calendarService = $calendarService;
        $this->templating = $templating;
    }

    /**
     * Basic controller for displaying calendars. Most likely
     * doesn't cover all scenarios.
     * @param string $view
     * @param string|DateTime $startDate
     * @param array $filters
     * @param integer $limit  Limit number of events pulled (does not place a limit on dates pulled)
     * @param string $template
     */
    public function viewCalendarAction(Request $request, $view = "month", $startDate = "", array $filters = array(), $limit = -1, $template = "")
    {

        // build response
        $response = new Response();
        $response->setPublic();
        $response->setSharedMaxAge(15);
        $response->setMaxAge(15);
        $response->setETag(
            md5( $view . (string) $startDate . json_encode($filters) . $template )
        );
        if ($response->isNotModified($request)) {
            return $response;
        }

        // provided view is an alias
        if (isset(self::$calendarViews[$view])) {
            $view = self::$calendarViews[$view];
        }

        // ensure view class exists
        if (!class_exists($view)) {
            throw new \InvalidArgumentException("Provided calendar view class does not exist.");
        }

        // create start date
        if (!$startDate) {
            $startDate = new \DateTime("now");
        } else {
            $startDate = new \DateTime($startDate);
        }

        // create filters
        $filterGroups = new CalendarGroupFilter(self::$filterGroupOperator);
        foreach ($filters as $filter) {
            if (!$filter) {
                continue;
            }
            $filterGroups->addCondition(
                CalendarGroupFilter::LOGIC_IN,
                $filter
            );
        }

        // create view
        $view = new $view($startDate, $filterGroups);

        // get event list
        $dateList = array();
        $eventList = array();
        foreach ($this->calendarService->getEventsInView($view) as $event) {
            $eventData = array(
                "event" => $event,
                "dates" => array()
            );
            foreach ($this->calendarService->getEventDatesInView($event, $view) as $date) {
                $dateList[] = array(
                    "date" => $date,
                    "event" => $event
                );
                $eventData["dates"][] = $date;
            }
            usort($eventData["dates"], function($a, $b) {
                if ($a == $b) {
                    return 0;
                }
                return $a < $b ? - 1 : 1;
            });
            $eventList[] = $eventData;
        }
        usort($dateList, function($a, $b) {
            if ($a["date"] == $b["date"]) {
                return 0;
            }
            return $a["date"] < $b["date"] ? - 1 : 1;
        });
        usort($eventList, function($a, $b) {
            if ($a["dates"][0] == $b["dates"][0]) {
                return 0;
            }
            return $a["dates"][0] < $b["dates"][0] ? - 1 : 1;
        });

        // @TODO find a way not to pull all events when limit is applied
        if ($limit > 0 && count($eventList) >= $limit) {
            $eventList = array_slice($eventList, 0, $limit);
        }

        // render output
        return $this->templating->renderResponse(
            $view->getTemplate() ?: self::DEFAULT_VIEW_TEMPLATE,
            array(
                "view" => $view,
                "dates" => $dateList,   // list of all dates with the event
                "events" => $eventList  // list of all events with dates
            ),
            $response
        );
         
    }
}
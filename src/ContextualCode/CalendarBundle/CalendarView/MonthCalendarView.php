<?php

namespace ContextualCode\CalendarBundle\CalendarView;
use Symfony\Component\HttpFoundation\Request;

use ContextualCode\CalendarBundle\Classes\CalendarGroupFilter;

class MonthCalendarView implements CalendarViewInterface
{

    protected $month;
    protected $year;
    protected $filter;

    public function __construct(\DateTime $currentDate, CalendarGroupFilter $filter = null)
    {
        $this->year = intval($currentDate->format("Y"));
        $this->month = intval($currentDate->format("n"));
        $this->filter = $filter;
    }

    public function getStartDate()
    {
        $date = new \DateTime();
        $date->setDate(
            $this->year,
            $this->month,
            1
        );
        $date->setTime(
            0,
            0,
            0
        );
        return $date;
    }

    public function getEndDate()
    {
        $date = new \DateTime();
        $date->setDate(
            $this->year,
            $this->month,
            $this->getStartDate()->format("t")
        );
        $date->setTime(
            23,
            59,
            59
        );
        return $date;
    }

    public function getNextPage()
    {
        return $this->getStartDate()->modify("+1 month");
    }

    public function getPrevPage()
    {
        return $this->getStartDate()->modify("-1 month");
    }

    public function getFilter()
    {
        return $this->filter;
    }

    public function getTemplate()
    {
        return "ContextualCodeCalendarBundle::month.html.twig";
    }

}
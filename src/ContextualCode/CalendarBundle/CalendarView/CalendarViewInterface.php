<?php

namespace ContextualCode\CalendarBundle\CalendarView;

use ContextualCode\CalendarBundle\Classes\CalendarGroupFilter;

/**
 * Interface which defines attributes for viewing events
 */
interface CalendarViewInterface
{

    /**
     * Constructor
     * @param DateTime $currentDate
     * @param CalendarFilter $filter
     */
    public function __construct(\DateTime $currentDate, CalendarGroupFilter $filter = null);

    /**
     * Get date to start view at
     * @return DateTime
     */
    public function getStartDate();

    /**
     * Get date to end view at
     * @return DateTime
     */
    public function getEndDate();

    /**
     * Get start date for next page
     * @return DateTime|null
     */
    public function getNextPage();

    /**
     * Get start date for previous page
     * @return DateTime|null
     */
    public function getPrevPage();

    /**
     * Get list of filters to apply to event
     * query
     * @return ContextualCode\CalendarBundle\Classes\CalendarGroupFilter
     */
    public function getFilter();

    /**
     * Get template path for view
     * @return string
     */
    public function getTemplate();

}
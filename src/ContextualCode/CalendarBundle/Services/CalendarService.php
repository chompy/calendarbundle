<?php

namespace ContextualCode\CalendarBundle\Services;

use ContextualCode\CalendarBundle\CalendarEventStorage\CalendarEventStorageInterface;
use ContextualCode\CalendarBundle\CalendarEvent\CalendarEventInterface;
use ContextualCode\CalendarBundle\CalendarEvent\CalendarCompiledEvent;
use ContextualCode\CalendarBundle\CalendarView\CalendarViewInterface;
use ContextualCode\CalendarBundle\Classes\CalendarGroupFilter;
use ContextualCode\CalendarBundle\Classes\EventCompiler;

class CalendarService
{

    public function __construct($compilePath, $kernelCacheDir)
    {
        if ($compilePath && is_dir($compilePath)) {
            EventCompiler::$compilePath = $compilePath;    
        } else {
            EventCompiler::$compilePath = $kernelCacheDir;    
        }
    }

    /**
     * Get events in given range
     * Event list is NOT sorted
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param CalendarGroupFilter $filter
     * @param integer $limit
     * @return CalendarEventInterface[]
     */
    public function getEvents(\DateTime $startDate = null, \DateTime $endDate = null, CalendarGroupFilter $filter = null, $limit = -1)
    {
        $eventList = EventCompiler::getCompiledEvents($startDate, $endDate, $limit);
        if (!$filter) {
            return $eventList;
        }
        $oEventList = array();
        foreach ($eventList as $event) {
            if ($filter->testFilter($event)) {
                $oEventList[] = $event;
            }
        }
        return $oEventList;
    }

    /**
     * Get events for a calendar view
     * @param CalendarViewInterface $view
     * @param integer $limit
     * @return CalendarEventInterface[]
     */
    public function getEventsInView(CalendarViewInterface $view, $limit = - 1)
    {
        return $this->getEvents(
            $view->getStartDate(),
            $view->getEndDate(),
            $view->getFilter(),
            $limit
        );
    }

    /**
     * Get all dates for given event within range
     * @param CalendarEventInterface $event
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param integer $limit
     * @return DateTime[]
     */
    static public function getEventDates(CalendarEventInterface $event, \DateTime $startDate = null, \DateTime $endDate = null, $limit = -1)
    {
        // compiled event
        if ($event instanceof CalendarCompiledEvent) {
            $dateList = EventCompiler::getCompiledDates($startDate, $endDate, $limit);
            $eventDateList = array();
            foreach ($dateList as $date) {
                if ($date[0] == $event->getId()) {
                    $eventDateList[] = $date[1];
                }
            }
            return $eventDateList;
        }

        // no rrule or rrule extension not loaded
        if (!$event->getRruleString() || !extension_loaded("cc-rrule")) {
            $eventStart = $event->getStartDate();
            $eventEnd = clone $eventStart;

            if ($event->getEventLength()) {
                $eventEnd->add($event->getEventLength());
            }

            if ($eventStart < $startDate && $eventEnd < $startDate) {
                return array();
            }
            if ($eventStart > $endDate && $eventEnd > $endDate) {
                return array();
            }
            return array($event->getStartDate());
        }
        
        $dates = array();
        $rrule = new \ContextualCode\Rrule(
            $event->getRruleString(),
            $event->getStartDate()
        );
        while ($date = $rrule->next()) {

            $eventEnd = clone $date;
            if ($event->getEventLength()) {
                $eventEnd->add($event->getEventLength());
            }

            if ($startDate && $date < $startDate && $eventEnd < $startDate) {
                continue;
            }
            if ($date > $endDate) {
                break;
            }
            $dates[] = $date;
            if ($limit > 0 && count($dates) >= $limit) {
                break;
            }

        }
        return $dates;


    }

    /**
     * Get all dates for event within given view
     * @param CalendarEventInterface $event
     * @param CalendarViewInterface $view
     * @param integer $limit
     * @return DateTime[]
     */
    static public function getEventDatesInView(CalendarEventInterface $event, CalendarViewInterface $view, $limit = -1)
    {
        return self::getEventDates(
            $event,
            $view->getStartDate(),
            $view->getEndDate(),
            $limit
        );
    }

}
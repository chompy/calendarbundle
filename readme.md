# Contextual Code Calendar Bundle

### 1) Setup

* Add ```ContextualCode\CalendarBundle\ContextualCodeCalendarBundle``` to the bundles list AppKernel.php.
* Run unit test to ensure everything is functional... ```phpunit -c app/ vendor/contextualcode/calendar-bundle/src/ContextualCode/CalendarBundle/Tests```
* (Optional) Install Contextual Code Rrule extension (https://bitbucket.org/chompy/rrrulephpextension). This is required in order to process reocurring events.

### 2) Usage

The Calendar Bundle includes an interface (```CalendarEventStorage\CalendarEventStorageInterface```) for retrieving calendar events. Currently the only storage mechanism shipped with this bundle is a PHP array storage. It's meant to be more of an example then a pratical storage mechanism. It should be a trivial matter to make a storage mechanism that connects to a database.

Once you have a proper calendar storage class defined you can create a service that uses the CalendarService (```Services\CalendarService```) as the class and the calendar storage class as the first argument.

See the ContextualCodeEzCalendarBundle (https://bitbucket.org/chompy/ezcalendarbundle) for a storage solution using eZ Publish.